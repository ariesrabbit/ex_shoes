import React, { Component } from "react";
import { data_shoes } from "./data_shoes";
import GioHang from "./GioHang";
import Item_Shoes from "./Item_Shoes";

export default class Main_Shoes extends Component {
  state = {
    shoes: data_shoes,
    gioHang: [],
  };
  renderContent = () => {
    return this.state.shoes.map((item) => {
      return <Item_Shoes handleAddToCart={this.handleAddToCart} data={item} />;
    });
  };
  handleAddToCart = (shoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index == -1) {
      let newSP = { ...shoe, soLuong: 1 };
      cloneGioHang.push(newSP);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  handleChangeSoLuong = (idShoe, value) => {
    let index = this.state.gioHang.findIndex((shoe) => {
      return (shoe.id = idShoe);
    });
    if (index == -1) return;
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + value;
    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <GioHang
            handleChangeSoLuong={this.handleChangeSoLuong}
            gioHang={this.state.gioHang}
          />
        )}
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
